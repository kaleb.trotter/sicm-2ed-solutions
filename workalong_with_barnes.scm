;; Workalong with Barnes

(define ((T-top A C))
    ((T-body-Euler A A C)
        (up 't
            (up 'theta 'varphi 'psi)
            (up 'thetadot 'varphidot 'psidot))))

(show-expression
    ((T-body-Euler 'A 'A 'C)
        (up 't
            (up 'theta 'varphi 'psi)
            (up 'thetadot 'varphidot 'psidot))))

(define ((V-top M g R) state)
    (let
        ((theta (ref (coordinate state) 0)))
        (* M g R (cos theta))
    ))

(show-expression ((V-top 'M 'g 'R) (up 't
            (up 'theta 'varphi 'psi)
            (up 'thetadot 'varphidot 'psidot))))

(define (L-top M g R A C)
    (- (T-top A C)
        (V-top M g R)))

(show-expression ((L-top 'M 'g 'R 'A 'C)
    (up 't
        (up 'theta 'varphi 'psi)
        (up 'thetadot 'varphidot 'psidot))))

(define (top_state_derivative M g R A C)
    (Lagrangian->state-derivative
        (L-top M g R A C)))

(show-expression ((top_state_derivative 'M 'g 'R 'A 'C)
    (up 't
        (up 'theta 'varphi 'psi)
        (up 'thetadot 'varphidot 'psidot))))