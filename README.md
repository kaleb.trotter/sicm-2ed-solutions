# Structure and Interpretation of Classical Mechanics, Second Edition

Solutions to the Scheme exercises presented in the second edition of Gerald Jay Sussman and Jack Wisdom's textbook, _Structure and Interpretation of Classical Mechanics_.

## Useful links
[Scmutils beginner's guide](http://groups.csail.mit.edu/mac/users/gjs/6946/beginner.pdf)

[Scmutils manual](http://groups.csail.mit.edu/mac/users/gjs/6946/refman.pdf)

[Patrick Catach's solutions to the first edition problems](https://github.com/pcatach/sicm-solutions)

[Sam Ritchie's solutions to second edition problems](https://github.com/hnarayanan/sicm-1)