;; HW 5.3: Project 1.44

; I copied your code entirely, then went through and tried to understand how it worked
; so that I could learn Scheme.

; Consider the ideal double pendulum shown in figure 1.11.

;; Part A
; Formulate a Lagrangian to describe the dynamics. Derive the equations of motion in
; terms of the given angles theta_1 and theta_2. Put the equations into a form appropriate for
; numerical integration.
(define g 9.8)  ; m / s^2
(define l1 1.0) ; m
(define l2 0.9) ; m
(define m1 1.0) ; kg
(define m2 3.0) ; kg

(define ((Kinetic m_1 m_2 l_1 l_2 g) local)
    (+ (* 1/2 m_1 (square ((v_1 m_1 m_2 l_1 l_2 g) local)))
        (* 1/2 m_2 (square ((v_2 m_1 m_2 l_1 l_2 g) local)))))

(define ((Potential m_1 m_2 l_1 l_2 g) local)
    (+ (* m_1 g ((y_1 m_1 m_2 l_1 l_2 g) local))
        (* m_2 g ((y_2 m_1 m_2 l_1 l_2 g) local))))

(define L_doublependulum (- Kinetic Potential))

(define ((y_1 m_1 m_2 l_1 l_2 g) local)
    (let ((t (time local))
        (q (coordinate local))
        (q_dot (velocity local)))
    (let ((theta_1 (ref q 0))           (theta_2 (ref q 1))
        (thetadot_1 (ref q_dot 0)) (thetadot_2 (ref q_dot 1)))
    (- (* l_1 (cos theta_1))))))

(define ((y_2 m_1 m_2 l_1 l_2 g) local)
    (let ((t (time local))
            (q (coordinate local))
            (q_dot (velocity local)))
    (let ((theta_1 (ref q 0))           (theta_2 (ref q 1))
        (thetadot_1 (ref q_dot 0)) (thetadot_2 (ref q_dot 1)))
    (- (-(* l_1 (cos theta_1)))
        (* l_2 (cos (+ theta_1 theta_2)))))))

(define ((v_1 m_1 m_2 l_1 l_2 g) local)
    (let ((t (time local))
            (q (coordinate local))
            (q_dot (velocity local)))
    (let ((theta_1 (ref q 0))       (theta_2 (ref q 1))
            (thetadot_1 (ref q_dot 0)) (thetadot_2 (ref q_dot 1)))
     (* l_1 thetadot_1))))

(define ((v_2 m_1 m_2 l_1 l_2 g) local)
    (let ((t (time local))
        (q (coordinate local))
        (q_dot (velocity local)))
    (let ((theta_1 (ref q 0))       (theta_2 (ref q 1))
        (thetadot_1 (ref q_dot 0)) (thetadot_2 (ref q_dot 1)))
     (sqrt (+
        (square (* l_1 thetadot_1))
        (* 2 l_1 l_2 thetadot_1 (+ thetadot_1 thetadot_2)
            (+ (* (sin theta_1) (sin (+ theta_1 theta_2)))
                (* (cos theta_1) (cos (+ theta_1 theta_2)))))
        (square (* l_2 (+ thetadot_1 thetadot_2))))))))

; (show-expression (((L_doublependulum 'm_1 'm_2 'l_1 'l_2 'g)
;     (up 't
;         (up (literal-function 'theta_1)
;             (literal-function 'theta_2))
;         (up (literal-function 'thetadot_1)
;             (literal-function 'thetadot_2))))
;     't))


; (show-expression (((Lagrange-equations (L_doublependulum 'm_1 'm_2 'l_1 'l_2 'g))
;     (up (literal-function 'theta_1)
;         (literal-function 'theta_2)))
;     't))


(define (system_state_derivative m_1 m_2 l_1 l_2 g)
    (Lagrangian->state-derivative
    (L_doublependulum m_1 m_2 l_1 l_2 g)))

; (show-expression ((system_state_derivative 'm_1 'm_2 'l_1 'l_2 'g)
;     (up 't
;         (up 'theta_1 'theta_2)
;         (up 'thetadot_1 'thetadot_2))))

(
    (system_state_derivative 1. 3. 1. 0.9 9.8)
    (up 0. (up :pi/2 :pi) (up 1.e-8 1.e-8))
)

((state-advancer system_state_derivative 1. 3. 1. 0.9 9.8)
    (up 0. (up :pi/2 :pi) (up 1.e-8 1.e-8))
        10.0
        1.e-12)

; (show-expression (
;     (system_state_derivative 1. 3. 1. 0.9 9.8)
;     (up 0. (up :pi/2 :pi) (up 1.e-8 1.e-8))
; ))

;; Part B
; Prepare graphs showing the behavior of each angle as a function of time when the
; system is started with the following initial conditions:
; 	theta_1(0) = pi/2 rad
; 	theta_2(0) = pi rad
; 	thetadot_1(0) = 0 rad/s
; 	thetadot_2(0) = 0 rad/s
; Make the graphs extend to 50 seconds.

(define plot-win1 (frame 0.0 50.0 :-pi :pi))

(define plot-win2 (frame 0.0 50.0 :-pi :pi))

(define ((plot-angles win) state)
    (let ((theta1 ((principal-value :pi) (ref (coordinate state) 0)))
        (theta2 ((principal-value :pi) (ref (coordinate state) 1))))
     (begin	(plot-point plot-win1 (time state) theta1)
         (plot-point plot-win2 (time state) theta1))))

((evolve system_state_derivative m1 m2 l1 l2 g)
    (up 0.0                 ; t0=0
        (up :pi/2 :pi )     ; theta_1(0)=pi/2, theta_2_0=pi
        (up 1.e-8 1.e-8 ))  ; thetadot=0 rad/s
    (plot-angles plot-win1)
        0.0001		 ;step size
        50.0		 ;final time
        1.0e-13)	 ; error tolerance

;; Part C
; Make a graph of the behavior of the energy of your system as a function of time. The
; energy should be conserved. How good is the conservation you obtained?
(define E_doublepend (+ Kinetic Potential))

(define plot-win3 (frame 0.0 50.0 -1.e-10 1.e-10))

(define ((plot-energy win) state)
    (let ((E ((E_doublepend 1. 3. 1. 0.9 9.8) state)))
        (plot-point win (time state) E)))

((evolve system_state_derivative m1 m2 l1 l2 g)
    (up 0.0                 ;t0=0
        (up :pi/2 :pi )     ;theta_1_0=pi/2, theta_2_0=pi
        (up 1.e-8 1.e-8 ))  ;thetadot=0 rad/s
    (plot-energy plot-win3)
        0.0001      ;step between plotted points
        50.0        ;final time
        1.0e-13)    ;local time error

;;	Part D
; Make a new Lagrangian, for two identical uncoupled double pendulums.
; Plot the logarithm of the absolute value of the difference of the positions of the m2
; bobs in your two pendulums against the time.
(define ((L_doubledoublepend m_1 m_2 m_3 m_4 l_1 l_2 l_3 l_4 g) state)
    (let ((t (time state)) (coord2 (coordinate state)) (vel2 (velocity state)))
        (let ((theta_0 (ref coord2 0)) (theta_1 (ref coord2 1))
            (theta_2 (ref coord2 2)) (theta_3 (ref coord2 3))
            (thetadot_0 (ref vel2 0)) (thetadot_1 (ref vel2 1))
            (thetadot_2 (ref vel2 2)) (thetadot_3 (ref vel2 3)))
    (let ((state1 (up t
            (up theta_0 theta_1)
            (up thetadot_0 thetadot_1)))
        (state2 (up t
            (up theta_2 theta_3)
            (up thetadot_2 thetadot_3))))
    (+ ((L_doublependulum m_1 m_2 l_1 l_2 g) state1)
        ((L_doublependulum m_3 m_4 l_3 l_4 g) state2))))))

(define (doubledoublepend-state-derivative m_1 m_2 m_3 m_4 l_1 l_2 l_3 l_4 g)
    (Lagrangian->state-derivative
    (L_doubledoublepend m_1 m_2 m_3 m_4 l_1 l_2 l_3 l_4 g)))

(define plot-win4 (frame 0.0 50.0 -35 3))

(define ((monitor-diff win) state)
    (let ((t (time state)) (coords (coordinate state)))
        (let ((first (ref coords 1)) (second (ref coords 3)))
        (let ((absdiff (abs (- second first))))
        (let ((logdiff (if (> absdiff 0.) (log absdiff) -50.) ))
            (plot-point win t logdiff))))))

((evolve doubledoublepend-state-derivative
    m1 m2
    m1 m2
    l1 l2
    l1 l2
    g)
    (up 0.0                         ;t0=0
        (up :pi/2 :pi :pi/2 :pi )   ;theta_1_0=pi/2, theta_2_0=pi
        (up 1.e-8 1.e-8 1.e-8 1.e-8 ))  ;thetadot=0 rad/s
    (monitor-diff plot-win4)
        0.001       ;step size
        50.0        ;final time
        1.0e-13)    ;local time error


;; Part E
; Repeat the previous comparison, but this time use the base initial conditions
(define plot-win5 (frame 0.0 50.0 -35 3))

((evolve doubledoublepend-state-derivative
    m1 m2
    m1 m2
    l1 l2
    l1 l2
    g)
    (up 0.0                     ;t0=0
        (up :pi/2 0 :pi/2 0 )   ;theta_1_0=pi/2, theta_2_0=0
        (up 1.e-8 1.e-8 1.e-8 1.e-8 ))  ;thetadot=0 rad/s
    (monitor-diff plot-win5)
        0.001       ;step size
        50.0        ;final time
        1.0e-13)    ;local time error