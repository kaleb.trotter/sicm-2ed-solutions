;; HW 4.4: Project 1.43

; I copied your code entirely, then went through and tried to understand how it worked
; so that I could learn Scheme.

; Consider a pendulum: a mass m supported on a massless rod of length l in a uniform
; gravitational field. A Lagrangian for the pendulum is
; 	L(t, theta, theta_dot) = (m/2) * (l * theta_dot)^2 + m*g*l*cos(theta)
; For the pendulum, the period of the motion depends on the amplitude. We wish to find
; trajectories of the pendulum with a given frequency. Three methods of doing this
; present themselves: (1) solution by the principle of least action, (2) numerical
; integration of Lagrange's equation, and (3) analytic solution (which requires some
; exposure to elliptic functions). We will carry out all three and compare the solution
; trajectories.

(define mass 1) 	; kg
(define len 1)		; m
(define g 9.8)	; m / s^2

(define ((L-pendulum m l g) local)
    (let ((theta (coordinate local))
            (theta_dot (velocity local)))
    (+ (* 1/2 m (square (* l theta_dot)))
        (* m g l (cos theta)))))

(show-expression (((Lagrange-equations
    (L-pendulum 'm 'l 'g))
        (literal-function 'theta))
            't))

;; Part A
; Find approximations to the first few coefficients An by minimizing the action. You will
; have to write a program similar to the find-path procedure in section 1.4. Watch out:
; there is more than one trajectory that minimizes the action.
(define win2 (frame 0.0 1.5 0 :pi))

(define ((parametric-path-action Lagrangian t0 q0 t1 q1)
        intermediate-qs)
    (let ((path (make-path t0 q0 t1 q1 intermediate-qs)))
        (Lagrangian-action Lagrangian path t0 t1)))

(find-path (L-pendulum mass len g)
    0.0 0. (* 1.5 (/ :pi (sqrt g))) 0. 3)

(define path1_5
    (find-path (L-pendulum mass len g) 0.0 0. (* 1.5 (/ :pi (sqrt g))) 0. 3))

(define path1_4
    (find-path (L-pendulum mass len g) 0.0 0. (* 1.4 (/ :pi (sqrt g))) 0. 3))

(define path1_3
    (find-path (L-pendulum mass len g) 0.0 0. (* 1.3 (/ :pi (sqrt g))) 0. 3))

(define path1_25
    (find-path (L-pendulum mass len g) 0.0 0. (* 1.25 (/ :pi (sqrt g))) 0. 3))

(define path1_2
    (find-path (L-pendulum mass len g) 0.0 0. (* 1.2 (/ :pi (sqrt g))) 0. 3))

(define path1_1
    (find-path (L-pendulum mass len g) 0.0 0. (* 1.1 (/ :pi (sqrt g))) 0. 3))

(define path1_01
    (find-path (L-pendulum mass len g) 0.0 0. (* 1.01 (/ :pi (sqrt g))) 0. 3))

(plot-function win2 path1_5 0 1.5 .01)

(plot-function win2 path1_4 0 1.4 .01)

(plot-function win2 path1_3 0 1.3 .01)

(plot-function win2 path1_2 0 1.2 .01)

(plot-function win2 path1_1 0 1.1 .01)

(plot-function win2 path1_01 0 1.01 .01)

; here's the answer
(path1_25 (* 1.25 (/ :pi/2 (sqrt g))))

; trying a fancy plot of A(omega)
(define win1 (frame 1.0 3.0 0.0 :pi))

(define (amplitude-from-period P)
    (let ((path
        (find-path (L-pendulum mass len g) 0.0 0. (* P (/ :pi (sqrt g))) 0. 3)))
    (path (* P (/ :pi/2 (sqrt g))))))

(amplitude-from-period 1.25)

(plot-function win1 amplitude-from-period 1.0 3.0 0.1)

;; Part b
; Write a program to numerically integrate Lagrange's equations for the trajectories of
; the pendulum.
(define ((monitor-theta win) state)
    (let ((theta ((principal-value :pi) (coordinate state))))
    (graphics-clear win)
    (plot-point win (time state) theta)))

(define ((no-op) state) (- 1 1))

(define win3 (frame 0. 5. :-pi :pi))

(define (system-state-derivative m l g)
    (Lagrangian->state-derivative (L-pendulum m l g)))

(show-expression ((system-state-derivative 'm 'l 'g) (up 't 'theta 'theta_dot)))

(define ((half-period theta_dot0) periodguess)
    (square (ref ((evolve system-state-derivative mass len g)
    (up 0. 0. theta_dot0)
    (no-op)
    0.01
    periodguess
    1.e-13) 1))
    )

(define (period-from-theta_dot0 theta_dot0)
 (* 2 (multidimensional-minimize (half-period theta_dot0) 1)))

(define (Omega-from-theta_dot0 theta_dot0)
 (/ (/ (* 2 :pi) (period-from-theta_dot0 theta_dot0)) (sqrt g) ))

(define win4 (frame 0. 5.5 0 1))

(plot-function win4 Omega-from-theta_dot0 0.1 5.5 0.1)




;; Part c
; Now let's formulate the analytic solution for the frequency as a function of
; amplitude.
(define ((theta_dot l g Amp) theta)
    (* (sqrt (/ (* 2 g) l))
        (sqrt (- (cos theta) (cos Amp)))))

(define ((integrand Amp) theta)
    (/ 4 ((theta_dot 1. 9.8 Amp) theta)))

(define ((period-from-amplitude) Amp)
    (definite-integral (integrand Amp) 0. (- Amp 1.0e-7)))

(define win5 (frame 0. :pi 0 8))

(plot-function win5 (period-from-amplitude) 0.01 (- :pi 0.01) 0.01)

(/ ((period-from-amplitude) 1.80511) ((period-from-amplitude) 0.02))
;#| 1.2546529653552267 |#