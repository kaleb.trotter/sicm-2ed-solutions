;; HW 4.2: Exercise 1.30

; I began with my own code, then brought your code in to make it work.
; I went through and tried to understand how your code worked so that I could learn
; Scheme.


; A Lagrangian for planar motion of a particle of mass m in a central field with
; potential energy V(r) = -beta*r^alpha is
;		L(t, r, theta; r_dot, theta_dot) = (1/2)*m*(r_dot^2 + (r*theta_dot)^2) + V(r)
(define ((Lagrange-equations Lagrangian) q)
  (- (D (compose ((partial 2) Lagrangian) (Gamma q)))
    (compose ((partial 1) Lagrangian) (Gamma q))))

(define (potential-energy r alpha beta)
  (- (* beta (expt r alpha))))

; a) Write a program to evolve the motion of a particle subject to this Lagrangian and
;		display the orbit in the plane.

(define ((L-central-potential m alpha beta) state)
 (let ((q (coordinate state))
       (qdot (velocity state)))
  (let ((r (ref q 0))
        (theta (ref q 1))
      (rdot (ref qdot 0))
      (thetadot (ref qdot 1)))
  (- (* 1/2 m (+ (square rdot) (square (* r thetadot))))
     (* beta (expt r alpha)))
)))

(define (central-potential-state-derivative m alpha beta)
  (Lagrangian->state-derivative
   (L-central-potential m alpha beta)))

(define ((monitor-orbit win) state)
  (let ((q (coordinate state)))
   (let ((r (ref q 0)) (theta (ref q 1)))
    (let ((x (* r (cos theta))) (y (* r (sin theta))))
     (plot-point win x y)))))

; b) Evolve this system with alpha = +2 (harmonic oscillator). Observe that it describes
;		an ellipse with its center at the origin, for a wide variety of initial
;		conditions.

(define spring-win (frame -1.2 1.2 -1.2 1.2))

((evolve central-potential-state-derivative 1. 2. 1.)
  (up 0. (up 1. 0.) (up 0. 1.))
    (monitor-orbit spring-win)
    0.01
    6.
    1.e-10)

((evolve central-potential-state-derivative 1. 2. 1.)
  (up 0. (up 0.5 0.5) (up 0.5 0.5))
    (monitor-orbit spring-win)
    0.01
    6.
    1.e-10)

((evolve central-potential-state-derivative 1. 2. 1.)
  (up 0. (up 0.75 -0.75) (up 0.5 0.5))
    (monitor-orbit spring-win)
    0.01
    6.
    1.e-10)

; c) Evolve this system with alpha = -1 (Newtonian gravity). Observe that it describes
;		an ellipse with a focus at the origin, for a wide variety of initial conditions.

(define kepler-win (frame -1.2 1.2 -1.2 1.2))

((evolve central-potential-state-derivative 1. -1. -1.)
  (up 0. (up 1. 0.) (up 0. 1.))
    (monitor-orbit kepler-win)
    0.01
    7.
    1.e-10)

((evolve central-potential-state-derivative 1. -1. -1.)
  (up 0. (up 0.5 0.5) (up 0.5 0.5))
    (monitor-orbit kepler-win)
    0.01
    6.
    1.e-10)

((evolve central-potential-state-derivative 1. -1. -1.)
  (up 0. (up 0.75 -0.75) (up 0.5 0.5))
    (monitor-orbit kepler-win)
    0.01
    6.
    1.e-10)

((evolve central-potential-state-derivative 1. -1. -1.)
  (up 0. (up .3 -0.85) (up 0.5 0.5))
    (monitor-orbit kepler-win)
    0.01
    6.
    1.e-10)

; d) Evolve this system with alpha = + 1/4. Observe that it describes a trefoil with its
;		center at the origin.

(define trefoil-win (frame -1.2 1.2 -1.2 1.2))

((evolve central-potential-state-derivative 1. 0.25 14.)
  (up 0. (up 1. 0.) (up 0. 1.))
    (monitor-orbit trefoil-win)
    0.001
    5.1
    1.e-13)

((evolve central-potential-state-derivative 1. 0.25 14.)
  (up 0. (up 0.5 0.5) (up 0.5 0.5))
    (monitor-orbit trefoil-win)
    0.0001
    4
    1.e-13)

((evolve central-potential-state-derivative 1. 0.25 14.)
  (up 0. (up 0.75 -0.75) (up 1.5 0.5))
    (monitor-orbit trefoil-win)
    0.00016
    8.0
    1.e-13)