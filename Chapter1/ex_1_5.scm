;; Pg. 14
(define ((L-free-particle mass) local)
    (let ((v (velocity local)))
        (* 1/2 mass (dot-product v v))))

;; pg 15
(define q
    (up (literal-function 'x)
        (literal-function 'y)
        (literal-function 'z)))

;; pg 16
((Gamma q) 't)

;; pg 17
(define (Lagrangian-action L q t1 t2)
    (definite-integral (compose L (Gamma q)) t1 t2))

;; pg 21
(define ((parametric-path-action Lagrangian t0 q0 t1 q1) qs)
    (let (( path (make-path t0 q0 t1 q1 qs)))
        (Lagrangian-action Lagrangian path t0 t1)))

;; pg 21
(define (find-path Lagrangian t0 q0 t1 q1 n)
    (let ((initial-qs (linear-interpolants q0 q1 n)))
        (let ((minimizing-qs
            (multidimensional-minimize
                (parametric-path-action Lagrangian t0 q0 t1 q1)
                initial-qs)))
            (make-path t0 q0 t1 q1 minimizing-qs))))

;; pg 22
(define ((L-harmonic m k) local)
    (let ((q (coordinate local))
           (v (velocity local)))
        (- (* 1/2 m (square v)) (* 1/2 k (square q)))))

;; pg 22
(define q
    (find-path (L-harmonic 1.0 1.0) 0.0 1.0 :pi/2 0.0 5))

;; pg 23
(define win2 (frame 0.0 :pi/2 0.0 1.2))
(define ((parametric-path-action Lagrangian t0 q0 t1 q1)
        intermediate-qs)
    (let ((path (make-path t0 q0 t1 q1 intermediate-qs)))
        ;; display path
        (graphics-clear win2)
        (plot-function win2 path t0 t1 (/ (- t1 t0) 100))
        ;; compute action
        (Lagrangian-action Lagrangian path t0 t1)))

;; pg 23
(find-path (L-harmonic 1.0 1.0) 0.0 1.0 :pi/2 0.0 5)