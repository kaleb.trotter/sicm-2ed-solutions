;; HW 2.6: Exercise 1.11
;; A Lagrangian suitable for studying the relative motion of two particles of masses m1
;; and m2 with potential energy V is defined below. The argument m is the reduced mass
;; of the system, and gravity's potential energy is given below with r as the distance
;; between two particles.
;;  Consider the simple situtation of the particles in circular orbits around their
;;	common center of mass. Construct a circular orbit and plug it into the Lagrange
;;	equations. Show that the residual gives Kepler's law: n^2 a^3 = G(m1 + m2) where
;;	n is the angular frequency of the orbit and a is the distance between particles.

;; SICM pg 33
(define ((Lagrange-equations Lagrangian) q)
	(- (D (compose ((partial 2) Lagrangian) (Gamma q)))
		(compose ((partial 1) Lagrangian) (Gamma q))))

;; From problem givens
(define ((L-central-polar m V) local)
	; Set the local tuple q
	(let ((q (coordinate local))
			; Set the velocity in the local tuple as qdot
			(qdot (velocity local)))
		; R is the reference position 0 of q, Phi is the reference position 1 of q
		(let ((r (ref q 0))        (phi (ref q 1))
			; R_dot is the reference position 0 of qdot,
			; Phi_dot is the reference position 1 of qdot
			  (rdot (ref qdot 0))  (phidot (ref qdot 1)))
			; This is the Lagrangian equation
			(- (* 1/2 m
				(+ (square rdot) (square (* r phidot))))
			(V r)))))

;; From problem givens
(define ((gravitational-energy G m_1 m_2) r)
	(- (/ (* G m_1 m_2) r)))

;; From problem givens
(define (mass_reduced m_1 m_2)
	(/ (* m_1 m_2) (+ m_1 m_2) ))

;; Set our coordinate system q
;;  q = {r, phi}
;; And propose a solution
(define ((proposed_solution r omega) t)
	(let ((phi (* omega t)))
		(up r phi)))

;; Now set the Lagrange equation, where r = a
(show-expression (((Lagrange-equations
		(L-central-polar (mass_reduced 'm_1 'm_2)
						(gravitational-energy 'G 'm_1 'm_2)))
		; a is the semi-major axis
		; n is the mean motion
		(proposed_solution 'a 'n))
	't))