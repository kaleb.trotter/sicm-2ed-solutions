;; HW 2.7: Exercise 1.12
;; Compute Lagrange's Equations for the Lagrangians in exercise 1.9 using the
;; Lagrange-equations procedure. Additionally, use the computer to perform each of the
;; steps in the Lagrange-equations procedure and show the intermediate results. Relate
;; these steps to the ones you showed in the hand derivation of exercise 1.9.
(define ((Lagrange-equations Lagrangian) q)
	(- (D (compose ((partial 2) Lagrangian) (Gamma q)))
		(compose ((partial 1) Lagrangian) (Gamma q))))

;; a) An ideal planar pendulum consists of a bob of mass m connected to a pivot by a
;; 		massless rod of length l subject to uniform gravitational acceleration g. A
;;		Lagrangian is,
;;			L(t,theta,theta_dot) = (1/2)*m*(l^2)*(theta_dot^2) + m*g*l*cos(theta)
;;		theta is the angle of pendulum rod to plumb line, theta_dot is the angular
;;		velocity of the rod

;; Begin by defining the pendulum's Lagrangian
(define ((L-pendulum m l g) local)
	; Set the position tuple q
	(let ((q (coordinate local))
			; Set the angular velocity tuple as q_dot
			(q_dot (velocity local)))
		; theta is the reference position 0 of q
		(let ((theta (ref q 0))
			; theta_dot is the reference position 0 of q_dot
			 (theta_dot (ref q_dot 0)))
			; This is the Lagrangian equation defined above
			(+ (* 1/2 m (square l) (square theta_dot)
				(* m g l (cos theta)))
			))))

(define ((proposed_solution q qdot) t)
	(up q qdot))

;; Now solve it
(show-expression (((Lagrange-equations
		(L-pendulum 'm 'l 'g))
		(proposed_solution 'theta 'theta_dot))
		't))

;; b) A particle of mass m moves in a two-dimensional potential
;;			V(x,y) = (x^2 + y^2)/2 + (x^2)*y - (y^3)/3
;;		where x, y are rectangular coordinates. A Lagrangian is,
;;			L(t;x,y;v_x,v_y) = (1/2)*m*(v_x^2 + v_y^2) - V(x,y)

; Define the potential energy function V
(define (potential_2d x y)
	(+ (/ (+ (square x) (square y)) 2)
		(* (square x) y)
		(- (/ (expt y 3) 3))))

; Define the particle's Lagrangian
(define ((L-particle-potential m V) local)
	; Set the position tuple q
	(let ((q (coordinate local))
			; Set the velocity tuple q_dot
			(q_dot (velocity local)))
		; x is the reference position 0 of q, y is the ref position 1
		(let ((x (ref q 0))				(y (ref q 1))
			; x_dot is the reference position 0 of q_dot
			 (x_dot (ref q_dot 0))		(y_dot (ref qdot 1)))
			; This is the Lagrangian equation defined above
			(- (* 1/2 m (+ (square x_dot) (square y_dot)))
				(V x y)
			))))

(show-expression ((Lagrange-equations
	(L-particle-potential 'm (potential_2d 'x 'y)))
	't))

;; c) A Lagrangian for a particle of mass m constrained on a sphere of radius R is
;; 		L(t;theta,phi;alpha,beta) = (1/2)*m*(R^2)*(alpha^2 + ((beta^2)*sin(theta)^2))
;;	The angle theta is the colatitude of the particle, phi is the longitude; the rate of
;;  change of the colatitude is alpha (alpha = theta_dot), the rate of change of the
;;	longitude is beta (beta = phi_dot)

; Define the particle's Lagrangian
(define ((L-particle-sphere m R) local)
	; Set the position tuple q
	(let ((q (coordinate local))
			; Set the velocity tuple q_dot
			(q_dot (velocity local)))
		; theta is the reference position 0 of q, phi is the ref position 1
		(let ((theta (ref q 0))			(phi (ref q 1))
			; alpha (theta_dot) is ref position 0 of q_dot,
			; beta (phi_dot) is ref position 1
			 (alpha (ref q_dot 0))		(beta (ref qdot 1)))
			; This is the Lagrangian equation defined above
			(* 1/2 m (square R)
				(+ (square alpha) (* (square beta) (square (sin theta)))))
			)))

(show-expression ((Lagrange-equations
	(L-particle-sphere 'm 'R ))
	't))