;; HW 9.1: Exercise 2.21

; In the '60s it was discovered that Mercury has a rotation period that is precisely
; 2/3 times its orbital period. We can see this resonant behavior in the spin-orbit
; model problem, and we can also play with nudging Mercury a bit to see how far off the
; rotation rate can be and still be trapped in this spin-orbit resonance. If the
; mismatch in angular velocity is too great, Mercury's rotation is no longer resonantly
; locked to its orbit. Set epsilon = 0.026 and e = 0.2.

; a. Write a program for the spin-orbit problem so this resonance dynamics can be
; investigated numerically. You will need to know (or, better, show!) that f satisfies
; the equation
;       Df(t) = n*(1 − e^2)^(1/2) (a / R(t))^2        (Eq 2.134)
; where
;       a / R(t) = (1 + e*cos(f(t))) / (1−e^2).       (Eq 2.135)

; b. Show that the 3:2 resonance is stable by numerically integrating the system when
; the rotation is not exactly in resonance and observing that the angle theta − 93/2)nt
; oscillates.

; c. Find the range of initial theta_dot for which this resonance angle oscillates.

;; Keplerian Orbits
;; Orbital calculator for use in Chapter 2 problem following the recipe from Solar
;; System Dynamics (Murray & Durmott) or Astrophysics Phys484/584 Lecture 2.3

;; Step 1: Calculate the mean anomaly, M=n(t-tau)
(define ((M n tau) state)
    (let ((t (time state)))
            (* n (- t tau))))

;; Step 2: Numerically solve Kepler's Equation, M=E-esinE, for E
(define ((Keplers_Equation e n tau t) E)
    (let ((thisM ((M n tau) (up t))))
        (- (- E thisM) (* e (sin E)))))

((Keplers_Equation 0.5 0.001 0. -1000.) -1.)

(define ((Eanomaly e n tau) state)
    (let ((t (time state)))
        (let ((thisM ((M n tau) state)) )
            (let ((offset (if (< thisM 0.) :pi 0.)))
                (ref
                    (minimize (square (Keplers_Equation e n tau t))
                        (- thisM :pi) (+ thisM :pi))
                    0)))))

((Eanomaly 0.5 0.001 0.) (up -20000.))

;; Step 3: Using E to find r & f
(define ((f_Kepler e n tau) state)
    (let ((E ((Eanomaly e n tau) state)))
        (let ((flip (if (< ((principal-value :pi) E) 0.) -1. 1.)))
            (* flip (acos (/ (- (cos E) e)
                    (- 1. (* e (cos E)))))))))

((f_Kepler 0.5 0.001 0.) (up 1000.))

(define ((r_Kepler e n tau a) state)
    (let ((f ((f_Kepler e n tau) state)))
        (/ (* a(- 1 (square e)))
            (+ 1 (* e (cos f))))))

(define win2 (frame -2. 2. -2. 2.))
(graphics-operation win2 'resize-window 1000 1000)
(graphics-clear win2)

(define ((xy_Kepler e n tau a) t)
    (let ((r ((r_Kepler e n tau a) (up t)))
        (f ((f_Kepler e n tau) (up t))) )
        (cons (* r (cos f))
            (* r (sin f))) ))

((xy_Kepler 0.5 0.001 0. 1.) 1000.)
(plot-parametric win2 (xy_Kepler 0.5 0.001 0. 1.) 0. 16280. 1.)

; Define our constants for Mercury and the Moon
; Mercury constants taken from:
;       https://solarsystem.nasa.gov/planets/mercury/by-the-numbers/
(define s_per_day 86400.)
(define M_Mercury 3.301e23)             ; kg
(define R_Mercury 2440000.)             ; Mean radius in meters
(define P_Mercury (* s_per_day 87.97))  ; seconds per cycle
(define n_Mercury (/ :2pi P_Mercury))   ; seconds per radian
(define a_Mercury 57909000000.)         ; meters
(define epsilon_Mercury 0.026)
(define e_Mercury 0.02)

(define M_Moon 7.348e22)            ; kg
(define R_Moon 1737100.)            ; Mean radius in meters
(define P_Moon (* s_per_day 27.3))  ; seconds per cycle
(define n_Moon (/ :2pi P_Moon))     ; seconds per radian
(define a_Moon 384000000.)          ; meters
(define epsilon_Moon 0.026)
(define e_Moon 0.0549)

(define win2 (frame 0. (* 50. P_Mercury) -1. 1.))
(graphics-operation win2 'resize-window 1000 1000)

; Solve Eq. 2.135
(define ((a_over_r e f) state)
    (/ (+ 1 (* e (cos f)))
	    (- 1 (square e))))

; Solve Eq. 2.134
(define ((f_dot e n) state)
    (let ((f (ref (coordinate state) 1)))
        (* n (sqrt (- 1. (square e))) (square ((a_over_r e f) state)))))

; Eq. 2.109, the averaged equation for motion in 1:1 resonance
(define ((thetadotdot_one_one_resonance n a epsilon e) state)
    (let ((t (time state))
        (theta (ref (coordinate state) 0))
        (thetadot (ref (velocity state) 0))
        (f (ref (coordinate state) 1)))
        (* -1 1/2 (square n) (square epsilon)
    	    (sin (* 2 (- theta f))) (cube ((a_over_r e f) state))  )))

; Eq. 2.118, the averaged equation for motion in 3:2 resonance
(define ((thetadotdot_three_two_resonance n a epsilon e) state)
    (let ((t (time state))
        (theta (ref (coordinate state) 0))
        (thetadot (ref (velocity state) 0))
        (f (ref (coordinate state) 1)))
        (* -1 1/2 (square n) (square epsilon) (/ (* 7 e) 2)
            (sin (- (* 2 theta) (* 3 f))))))

; Run the numbers on the averaged equation for motion for the moon
((thetadotdot_one_one_resonance n_Moon a_Moon epsilon_Moon e_Moon)
    (up 0. (up 0.1 0.) (up (* 1.01 n_Moon) n_Moon)))

; Run the numbers on the averaged equation for motion for the Mercury
((thetadotdot_three_two_resonance n_Mercury a_Mercury epsilon_Mercury e_Mercury)
    (up 0. (up 0.1 0.) (up (* 1.01 n_Mercury) n_Mercury)))

; Run the numbers on the averaged equation for motion on the moon
(define ((theta-state-derivative n a epsilon e) state)
    (up 1.
        ; replace P_Moon with P_Moon if trying to fix aritherror
        (up
            (* 1. (ref (velocity state) 0))
            (* 1. ((f_dot e n) state)))
        (up
            (* (square 1.) ((thetadotdot n a epsilon e) state))
            0.0)))

; Run the numbers on the averaged equation for motion on Mercury
(define ((theta-three-two-state-derivative n a epsilon e) state)
    (up 1.
        ; replace P_Mercury with P_Mercury if trying to fix aritherror
        (up
            (* 1. (ref (velocity state) 0))
            (* 1. ((f_dot e n) state)))
        (up
            (* (square 1.) ((thetadotdot_three_two_resonance n a epsilon e) state))
            0.0)))

((theta-state-derivative n_Moon a_Moon epsilon_Moon e_Moon)
    (up 0. (up 0. 0.1) (up (* 1.01 n_Moon) n_Moon) ))

((theta-three-two-state-derivative n_Mercury a_Mercury epsilon_Mercury e_Mercury)
    (up 0. (up 0. 0.1) (up (* 1.01 n_Mercury) n_Mercury) ))

; Show subsolar for the 1:1 resonance
(define ((show-subsolar win) state)
    (let (
        (t (time state))
        (theta (ref (coordinate state) 0))
        (f (ref (coordinate state) 1)))
        (let ((phi (- theta f)))
            (graphics-operation win2 'set-foreground-color "white")
            (plot-point win t ((principal-value :pi) phi)))))

; Show subsolar for the 3:2 resonance
(define ((show-subsolar-three-two win) state)
    (let (
        (t (time state))
        (theta (ref (coordinate state) 0))
        (f (ref (coordinate state) 1)))
        (let ((phi (- (* 2 theta) (* 3 f))))
            (graphics-operation win2 'set-foreground-color "white")
            (plot-point win t ((principal-value :pi) phi)))))

;; need to run this separately; can't just cut-and-paste or the window doesn't come out right
(graphics-clear win2)

((evolve theta-state-derivative n_Moon a_Moon epsilon_Moon e_Moon)
    (up 0.
        (up 0.0 0.0)
        (up (* 1.01 n_Moon) 0.))
    (show-subsolar win2)
    (* 0.1 s_per_day)
    (* 50. P_Moon)
    1.e-7)

((evolve theta-three-two-state-derivative n_Mercury a_Mercury epsilon_Mercury e_Mercury)
    (up 0.
        (up 0.0 0.0)
        (up (* 1.01 n_Mercury) 0.))
    (show-subsolar-three-two win2)
    (* 0.1 s_per_day)
    (* 50. P_Mercury)
    1.e-7)
