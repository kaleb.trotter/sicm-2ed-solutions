;; Project 2.20: Free rigid body

; I started with my own code, then had to copy and paste in most of yours when mine
; didn't work the way I expected.

; Write and demonstrate a program that reproduces diagrams like figure 2.3 (section
; 2.8.2). Can you find trajectories that are asymptotic to the unstable relative
; equilibrium on the intermediate principal axis?

(define Euler_state
    (up 't
        (up 'theta 'varphi 'psi)
        (up 'thetadot 'varphidot 'psidot)))

(define Lagrange_in_Euler ((L-space-Euler 'A 'B 'C) Euler_state))

(define D2T_body_Euler (((partial 2) (T-body-Euler 'A 'B 'C)) Euler_state))

(- (ref Lagrange_in_Euler 2) (ref D2T_body_Euler 1))

(define (rigid_system_der A B C)
    (Lagrangian->state-derivative (T-body-Euler A B C)))

; Using Bulirsch-Stoer
(set-ode-integration-method! 'qcrk4)

; Set up the plotting window from 0 to 400 in Y, -0.4 to 0.4 in X
(define win2 (frame 0.0 400.0 -0.4 0.4))
(graphics-operation win2 'set-background-color "white")
(graphics-operation win2 'resize-window 1000 1000)
(graphics-clear win2)

; Now set up the plotting function with red, blue, and green lines
(define ((show_momenta win A B C) state)
    (let ((t (time state))
        (L ((L-body-Euler A B C) state))
            (E ((T-body-Euler A B C) state)))
    (graphics-operation win2 'set-foreground-color "red")
    (plot-point win t (ref L 0))
    (graphics-operation win2 'set-foreground-color "#00aa00")
    (plot-point win t (ref L 1))
    (graphics-operation win2 'set-foreground-color "blue")
    (plot-point win t (ref L 2))))

; Create rotations around axis C
(graphics-clear win2)
(let (
    (A 1.0)
    (B (sqrt 2.0))
    (C 2.0)
    (initial_state
        (up 0.0
            (up 1.57 0.0 0.0)
            (up 0.0 0.0 0.2)
        )))
    (let (
        (L_min 0.2)
        (L_max 400.0)
        (L_step 1.0e-12)
        (L_0 ((L-space-Euler A B C) initial_state))
        (E_0 ((T-body-Euler A B C) initial_state)))
        ((evolve rigid_system_der A B C)
            initial_state
            (show_momenta win2 A B C)
            L_min
            L_max
            L_step)))

; Create rotations around axis A
(graphics-clear win2)
(let (
    (A 1.0)
    (B (sqrt 2.0))
    (C 2.0)
    (initial_state
        (up 0.0
            (up 1.e-2 0.0 0.0)
            (up 0.2 0.0 0.0)
        )))
    (let (
        (L_min 0.2)
        (L_max 400.0)
        (L_step 1.0e-12)
        (L_0 ((L-space-Euler A B C) initial_state))
        (E_0 ((T-body-Euler A B C) initial_state)))
        ((evolve rigid_system_der A B C)
            initial_state
            (show_momenta win2 A B C)
            L_min
            L_max
            L_step)))

; Rotation around b
(graphics-clear win2)
(let (
    (A 1.0)
    (B (sqrt 2.0))
    (C 2.0)
    (initial_state
        (up 0.0
            (up 1.57 0.0 0.0)
            (up 0.0 0.25 0.0)
        )))
    (let (
        (L_min 0.2)
        (L_max 400.0)
        (L_step 1.0e-12)
        (L_0 ((L-space-Euler A B C) initial_state))
        (E_0 ((T-body-Euler A B C) initial_state)))
        ((evolve rigid_system_der A B C)
            initial_state
            (show_momenta win2 A B C)
            L_min
            L_max
            L_step)))

; Now create the full body momentum vector in 3D
(define win3 (frame -0.13 0.13 -0.13 0.13))
(graphics-operation win3 'set-background-color "white")
(graphics-operation win3 'set-foreground-color "black")
(graphics-operation win3 'resize-window 1000 1000)
(graphics-clear win3)

(define (Rz-mat angle) (matrix-by-rows
    (list   (cos angle)   (- (sin angle)) 0)
    (list   (sin angle)   (cos angle)     0)
    (list   0             0               1)))

(define (Rx-mat angle) (matrix-by-rows
    (list   1             0               0)
    (list   0             (cos angle)     (- (sin angle)))
    (list   0             (sin angle)     (cos angle))))

(define (Euler->M angles)
    (let ((theta (ref angles 0))
        (phi   (ref angles 1))
        (psi   (ref angles 2)))
        (* (Rz-mat phi)
            (Rx-mat theta)
            (Rz-mat psi))))

(define ((reproject_to_m angles) coords)
  (* (Euler->M angles) coords)
))

(define subspacecraft_point (up 0.5 0. 0.2))

(define ((down2up) state)
    (up (ref state 0) (ref state 1) (ref state 2)))

; Change coordinates to moment of inertia system
((reproject_to_m subspacecraft_point) (up 1. 1. 1.))
((reproject_to_m subspacecraft_point) (up 1. 1. 1.))

; Define our Lagrangian
(define L ((L-body-Euler 1.0 1.41 2.0)
    (up 0.0
        (up 0.5 0.0 0.0)
        (up 0.2 -0.25 0.25))
    ))

; Define the plotting function
(define ((momenta3D win A B C) state)
    (let ((t (time state))
        (L ((L-body-Euler A B C) state)))
    (let ((L_projected ((reproject_to_m subspacecraft_point) ((down2up) L))))
        (let ((x (ref L_projected 0))
            (y (ref L_projected 1))
            (z (ref L_projected 2)))
        (if (> 0. y)
            (graphics-operation win 'set-foreground-color "red")
            (graphics-operation win 'set-foreground-color "pink"))
        (plot-point win x z)))))

(graphics-clear win3)
(define A 1.)
(define B (sqrt 2.))
(define C 2.)
(define L_min 0.3)
(define L_max 400.0)
(define L_step 1.0e-12)

(define (plottrajectory initial_state)
    ((evolve rigid_system_der A B C)
        initial_state
        (momenta3D win3 A B C)
        L_min
        L_max
        L_step))

(define initial_state
    (up 0.0
        (up 2. 0.0 0.0)
        (up +0.055 -0.0505 -0.02)))

((T-body-Euler 1. (sqrt 2.) 2.) initial_state)

; Now plot the lines
(plottrajectory (up 0.0 (up 2 0.0 0.0) (up -0.067 +0.0275 0.03)))
(plottrajectory (up 0.0 (up 2 0.0 0.0) (up +0.067 +0.0275 0.03)))
(plottrajectory (up 0.0 (up 2 0.0 0.0) (up -0.0778 0.0 0.0)))
(plottrajectory (up 0.0 (up 2 0.0 0.0) (up +0.0778 0.0 0.0)))
(plottrajectory (up 0.0 (up 2 0.0 0.0) (up -0.075 +0.01 0.015)))
(plottrajectory (up 0.0 (up 2 0.0 0.0) (up +0.075 +0.01 0.015)))
(plottrajectory (up 0.0 (up 2 0.0 0.0) (up +0.044 +0.04 0.05)))
(plottrajectory (up 0.0 (up 2 0.0 0.0) (up 0. 0. 0.055)))
(plottrajectory (up 0.0 (up 2 0.0 0.0) (up 0. 0. -0.055)))
(plottrajectory (up 0.0 (up 2 0.0 0.0) (up 0.01 -0.0090 0.05)))
(plottrajectory (up 0.0 (up 2 0.0 0.0) (up 0.030 -0.02 0.04)))
(plottrajectory (up 0.0 (up 2 0.0 0.0) (up 0.030 +0.02 -0.04)))
(plottrajectory (up 0.0 (up 2 0.0 0.0) (up 0.007 +0.01 -0.05)))
(plottrajectory (up 0.0 (up 2 0.0 0.0) (up 0.0775 +0.0 -0.03)))
(plottrajectory (up 0.0 (up 2 0.0 0.0) (up -0.0775 +0.0 -0.03)))
(plottrajectory (up 0.0 (up 2 0.0 0.0) (up 0.055 +0.0505 -0.02)))

