;; HW 8.3: Exercise 2.16, Part E

; Consider a top that is rotating so that theta is constant.

;; Part E
; Numerically integrate the top to check your deductions
(define ((T-top A C) state)
    ((T-body-Euler A A C) state))

(define ((V-top M g R) state)
    (let
        ((theta (ref (coordinate state) 0)))
        (* M g R (cos theta))
    ))

(define (L-top M g R A C)
    (- (T-top A C) (V-top M g R))
    )

(define (top_state_derivative M g R A C)
    (Lagrangian->state-derivative
        (L-top M g R A C)))

(show-expression ((top_state_derivative 'M 'g 'R 'A 'C)
    (up 't
        (up 'theta 'varphi 'psi)
        (up 'thetadot 'varphidot 'psidot))))

(define win2 (frame 0.0 2.0 0 :2pi))
(graphics-operation win2 'set-background-color "white")
(graphics-operation win2 'resize-window 1000 1000)
(graphics-clear win2)

; Set up the plotting window and line colors
(define ((show-eulers win) state)
    (let (
        (t (time state))
        (phi ((principal-value :2pi) (ref (coordinate state) 1) ))
    )
    (graphics-operation win2 'set-foreground-color "red")
    (plot-point win t (ref (coordinate state) 0))
    (graphics-operation win2 'set-foreground-color "#00aa00")
    (plot-point win t phi)
    (graphics-operation win2 'set-foreground-color "blue")
    (plot-point win t (/ (ref (velocity state) 2) 100))
    ))

(let (
    ; Define variables
    (g 9.81)
    (M 0.114169)
    (R 0.1)
    (C 1.32e-4)
    (A 6.96e-4)
    (state_initial
        (up 0.
            (up 0.4 0. 0.)
            (up 0.0 -10.0 200.0)
        ))
    (t_initial 0.001)
    (t_final 2.0)
    (t_step 1.0e-12))
    ; Now run the graph
    ((evolve top_state_derivative M g R A C)
        state_initial
        (show-eulers win2)
        t_initial
        t_final
        t_step))


